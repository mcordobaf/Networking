//
//  NetworkProvider.swift
//  Networking
//
//  Created by Marco Córdoba Fernández on 02-08-21.
//

import Foundation

public protocol MainFetcher:AnyObject{
  var networkProvider: NetworkProviderProtocol { get }
}

public protocol NetworkProviderProtocol
{
  var network:NetworkProtocol { get }
}

public final class NetworkProvider: NetworkProviderProtocol {
  public var network: NetworkProtocol
  init(network: NetworkProtocol = NetworkExecutor.builder()) {
    self.network = network
  }
  
  public static func builder() -> NetworkProviderProtocol{
    return NetworkProvider()
  }
}
