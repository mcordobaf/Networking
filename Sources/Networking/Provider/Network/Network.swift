//
//  Network.swift
//  Networking
//
//  Created by Marco Córdoba Fernández on 25-07-21.
//

import Foundation
import Combine

public protocol FetcherProtocol{
  func forecast<Body:Codable, T:Codable>(_ generator:RequestGenerator<Body>) -> AnyPublisher<T, GenericError>
}

public protocol NetworkProtocol:FetcherProtocol{
  var session:URLSession { get }
}

public final class NetworkExecutor:NetworkProtocol
{
  
  public var session:URLSession
  public init() {
    let configuration = URLSessionConfiguration.default
    configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
    self.session = URLSession(configuration: configuration)
  }
  
  public static func builder() -> NetworkProtocol{
    return NetworkExecutor()
  }
  
  private func processErrorToGeneric(errorValidation:ValidationError?) -> GenericError{
    switch errorValidation {
    case .errorStatusCode(let error, let statusCode):
      return GenericError.statusCodeError(statusCodeError: statusCode, error: error)
    default:
      return .network(description: errorValidation?.localizedDescription ?? Constants.DEFAULT_STRING)
    }
  }
  
  public func forecast<Body, T>(_ generator: RequestGenerator<Body>) -> AnyPublisher<T, GenericError> where Body:Codable, T : Codable {
    guard let url = generator.url else {
      let error = GenericError.network(description: "Couldn't create URL")
      return Fail(error: error).eraseToAnyPublisher()
    }

    var urlRequest = URLRequest(url: url)
    urlRequest.setValue(Constants.CONTENT_TYPE_JSON_VALUE, forHTTPHeaderField: Constants.CONTENT_TYPE_KEY)
    if let headers = generator.headers{
      LoggerUtils.log(text: "Headers :")
      for (key, value) in headers{
        LoggerUtils.log(text: "\(key)=\(value)")
        urlRequest.setValue(value, forHTTPHeaderField: key)
      }
    }
    
    if let auth = generator.authentication.getAuthenticationHeader(), let firstValue = auth.first{
      urlRequest.setValue(firstValue.value, forHTTPHeaderField: firstValue.key)
    }
    urlRequest.httpMethod = generator.method.rawValue

    if let body = generator.getBody(){
      urlRequest.httpBody = body
      LoggerUtils.log(text: "Body: \(String(data: body, encoding: .utf8) ?? "")")
    }
    
    debugPrint(urlRequest)
    let id = UUID()
    debugPrint("--> START REQUEST with id request: \(id)")

    return session.dataTaskPublisher(for: urlRequest)
      .validateStatusCode({(200..<300).contains($0)})
      .validateResponse {
        #if DEBUG
        let dataResponse = String(decoding: $0.data, as: UTF8.self)
        debugPrint("--> id request: \(id)")
        debugPrint("--> START DATA RESPONSE")
        debugPrint(dataResponse)
        debugPrint("--> END DATA RESPONSE")
        #endif
        return !$0.data.isEmpty
      }
      .mapJsonError(to: ApiErrorResponse.self, decoder: JSONDecoder())
      .mapError { error in
        return self.processErrorToGeneric(errorValidation: error as? ValidationError)
      }
      .flatMap(maxPublishers: .max(1)) { pair in
        decode(pair.data)
      }
      .eraseToAnyPublisher()
  }
}
