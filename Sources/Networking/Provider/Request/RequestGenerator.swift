//
//  RequestGenerator.swift
//  Networking
//
//  Created by Marco Córdoba Fernández on 25-07-21.
//

import Foundation

public typealias Parameters = [String: Any]
public typealias Headers = [String: String]

public protocol RequestGeneratorProtocol{
  var method:RequestMethod { get }
  var headers:Headers? { get }
  var parameters:Parameters? { get }
  var authentication:Authentication{ get }
  var url:URL? { get }
}

public class RequestGenerator<Body>:RequestGeneratorProtocol where Body:Codable{
  public var method: RequestMethod
  public var headers: Headers?
  public var parameters: Parameters?
  public var authentication: Authentication
  public var url:URL?
  private var body: Body?

  public init(urlComponents:URLComponents,
              method:RequestMethod,
              headers:Headers? = nil,
              parameters:Parameters? = nil,
              authentication:Authentication = Authentication(authenticationMethod: .none),
              body:Body? = nil)
  {
    self.url = urlComponents.url
    self.method = method
    self.headers = headers
    self.parameters = parameters
    self.authentication = authentication
    self.body = body
  }
  
  public func getBody() -> Data? where Body:Codable {
    if let body = self.body, let bodyEncoded = try? JSONEncoder().encode(body){
      return bodyEncoded
    }
    return nil
  }
}
