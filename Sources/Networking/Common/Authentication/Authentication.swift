//
//  File.swift
//  
//
//  Created by Marco Córdoba Fernández on 02-07-21.
//

import Foundation

protocol AuthenticationProtocol
{
  var authenticationMethod:AuthenticationMethod { get }
  func getAuthenticationHeader() -> [String:String]?
}

public class Authentication: AuthenticationProtocol
{
  var authenticationMethod: AuthenticationMethod
  
  public init(authenticationMethod:AuthenticationMethod) {
    self.authenticationMethod = authenticationMethod
  }
  
  func getAuthenticationHeader() -> [String:String]? {
    switch authenticationMethod {
    case .bearerToken(let bearerToken):
      return [Constants.AUTHORIZATION_KEY : bearerToken ?? ""]
    case .basicAuthentication(let basicAuth):
      return [Constants.AUTHORIZATION_KEY : basicAuth ?? ""]
    default:
      return nil
    }
  }
}

public enum AuthenticationMethod{
  case basicAuthentication(_ basicAuthentication:String?)
  case bearerToken(_ bearerToken:String?)
  case none
}
