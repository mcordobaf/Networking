//
//  File.swift
//  
//
//  Created by Marco Córdoba Fernández on 02-07-21.
//

import Foundation

struct Constants{
  static let CONTENT_TYPE_JSON_VALUE = "application/json"
  static let CONTENT_TYPE_KEY = "Content-Type"
  static let AUTHORIZATION_KEY = "Authorization"
  static let DEFAULT_STRING = ""
}
