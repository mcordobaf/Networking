//
//  File.swift
//  
//
//  Created by Marco Córdoba Fernández on 02-07-21.
//

import Foundation

public enum RequestMethod:String{
  case get = "GET"
  case post = "POST"
  case delete = "DELETE"
}
