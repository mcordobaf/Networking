//
//  GenericError.swift
//  ParisApp
//
//  Created by Henry AT on 25-01-21.
//

import Foundation

public enum GenericError: Error {
  case parsing(description: Error)
  case network(description: String)
  case notFound(error:Error)
  case statusCodeError(statusCodeError:Int, error:Error)
}
