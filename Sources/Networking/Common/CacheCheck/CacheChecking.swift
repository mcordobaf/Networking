//
//  CacheChecking.swift
//  ParisApp
//
//  Created by Marco Córdoba Fernández on 08-04-21.
//

import Foundation

@available(iOS 14.0, *)
@propertyWrapper
public class CacheCheckingClass<T> {
  private let userDefaults:UserDefaults = UserDefaults.standard
  private let key:KeyCache
  init(key:KeyCache) {
    self.key = key
  }
  public var wrappedValue:T? {
    get {
      userDefaults.object(forKey: self.key.rawValue) as? T
    }
    set{
      userDefaults.setValue(newValue, forKey: self.key.rawValue)
      userDefaults.synchronize()
    }
  }
}

@available(iOS 14.0, *)
public struct CacheCheckingStruct{
  public static let shared = CacheCheckingStruct()
  @CacheCheckingClass(key: KeyCache.timeout) public var timeout:Int?
}

enum KeyCache: String{
  case timeout
}
