//
//  Parsing.swift
//  ParisApp
//
//  Created by Henry AT on 25-01-21.
//

import Foundation
import Combine

public func decode<T: Decodable>(_ data: Data) -> AnyPublisher<T, GenericError> {
  let decoder = JSONDecoder()
  decoder.dateDecodingStrategy = .secondsSince1970

  return Just(data)
    .decode(type: T.self, decoder: decoder)
    .mapError { error in
      .parsing(description: error)
    }
    .eraseToAnyPublisher()
}
