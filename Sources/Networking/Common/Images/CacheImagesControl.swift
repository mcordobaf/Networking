//
//  File.swift
//  ParisApp
//
//  Created by Marco Córdoba Fernández on 07-04-21.
//


import Foundation
import SwiftUI
import Combine

public class CacheImagesControl
{
  public static let shared = CacheImagesControl()
  let imageCache = NSCache<NSString, AnyObject>()
  public var disposibles = Set<AnyCancellable>()
  init() {
    self.imageCache.countLimit = 200
  }
  
  public func addCacheData(key:String, data:AnyObject){
    imageCache.setObject(data, forKey: key as NSString)
  }
  
  public func getImageFromCache(key:String) -> UIImage?{
    if let image = imageCache.object(forKey: key as NSString) as? UIImage
    {
      return image
    }
    return nil
  }
}
