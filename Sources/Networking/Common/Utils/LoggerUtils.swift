//
//  LoggerUtils.swift
//  ParisApp
//
//  Created by Marco Córdoba Fernández on 25-07-21.
//

import Foundation

class LoggerUtils{
  static func log(text:String){
    #if DEBUG
    print("\(text)")
    #endif
  }
}
